package com.acount.acountdataservice.web;

import com.acount.acountdataservice.feign.CustomerRestClient;
import com.acount.acountdataservice.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;
import org.springframework.graphql.client.HttpGraphQlClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Flux;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/account-service")
public class AccountRestController {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CustomerRestClient customerRestClient;


    @GetMapping("/customers")
    public List<Customer> listCustomer(){
        Customer[] customers = restTemplate.getForObject("http://localhost:8082/customers", Customer[].class);
        return List.of(customers);
    }

    // restTemplate avec paramètre dans l'URL getById
    @GetMapping("/customer/v1/{id}")
    public Customer customerById (@PathVariable Long id){
        Customer customer = restTemplate.getForObject("http://localhost:8082/customer/" +id, Customer.class);
        return customer;
    }


    // webFlux get liste of customers en utlise le flux qui nous retourne un ensemble des element en temps reél
    @GetMapping("/webflux/customers")
    public Flux<Customer> fluxCustomers(){
        WebClient webClient = WebClient
                .builder()
                .baseUrl("http://localhost:8082")
                .build();


        Flux<Customer> customerFlux = webClient.get()
                .uri("/customers")
                .retrieve()
                .bodyToFlux(Customer.class);

        return customerFlux;
    }

    // WebFlux avec paramètre en utlise mono pour le retour d'un seul élément
    @GetMapping("/customer/v2/{id}")
    public Mono<Customer> customerByIdV2(@PathVariable Long id){
        WebClient webClient = WebClient
                .builder()
                .baseUrl("http://localhost:8082")
                .build();

        Mono<Customer> customerMono = webClient.get()
                .uri("/customer/{id}", id)
                .retrieve()
                .bodyToMono(Customer.class);

        return customerMono;
    }


    @GetMapping("/feign/customers")
    public List<Customer> listCustomerv3(){
        return customerRestClient.getCustomers();
    }

    //openFeign avec paramètre
    @GetMapping("/customer/v3/{id}")
    public Customer listCustomerById(@PathVariable Long id){
        return customerRestClient.getCustomerById(id);
    }


    // get customer by id avec le graphQl et webClient
    @GetMapping("/gql/customers/{id}")
    public Mono<Customer> getCustomersByIdGql(@PathVariable Long id){
        HttpGraphQlClient graphQlClient=HttpGraphQlClient.builder()
                .url("http://localhost:8082/graphql")
                .build();
        var httpRequestDocument= """
                 query($id:Int) {
                    customerById(id:$id){
                      id, name, email
                    }
                  }
                """;
        Mono<Customer> customerById = graphQlClient
                .document(httpRequestDocument)
                .variable("id",id)
                .retrieve("customerById")
                .toEntity(Customer.class);

        return customerById;
    }
    @GetMapping("/gql/customers")
    public Mono<List<Customer>> getAllCustomers(){
        HttpGraphQlClient graphQlClient=HttpGraphQlClient.builder()
                .url("http://localhost:8082/graphql")
                .build();
        var httpRequestDocument= """
                query {
                  allCustomer{
                    id, name, email
                  }
                }
                """;
        Mono<List<Customer>> customer = graphQlClient
                .document(httpRequestDocument)
                .retrieve("allCustomer")
                .toEntityList(Customer.class);

        return customer;
    }


}
